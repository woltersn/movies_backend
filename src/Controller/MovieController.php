<?php
namespace App\Controller;

use Google;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Dialogflow\WebhookClient;

class MovieController extends ApiController
{
    /**
    * @Route("/movies")
    */
    public function moviesAction()
    {
        return $this->respond([
            [
                'title' => 'The Princess Bride',
                'count' => 0
            ]
        ]);
    }

     /**
    * @Route("/token")
    */
    public function googleToken()
    {

        $client = new Google\Client();
        $client->setApplicationName("Chatbot");
        $client->setDeveloperKey("AIzaSyCXhG8ujwcOzJGPRkr23L274P4Dtjt3GKM");
        $client->setAuthConfig(__Dir__ . '\..\chatbot.json');
        $client->addScope(array('https://www.googleapis.com/auth/cloud-platform','https://www.googleapis.com/auth/dialogflow'));
        $client->setRedirectUri('https://localhost:8000');
        $client->setPrompt('consent');
        $client->setIncludeGrantedScopes(true);
        $client->setAccessType('offline');

        $token = $client->fetchAccessTokenWithAssertion();

        return $this->respond(
            $token
        );
    }

    /**
    * @Route("/dialogflow", methods={"POST"})
    */
    public function webhook(Request $request){
        $agent = new WebhookClient(json_decode(file_get_contents('php://input'),true));
        $agent->reply("Scheise da.");
        header('Content-type: application/json');

        return $this->respond(
            $agent->render()
        );
    }
}

