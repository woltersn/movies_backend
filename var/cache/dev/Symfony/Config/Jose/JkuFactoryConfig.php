<?php

namespace Symfony\Config\Jose;


use Symfony\Component\Config\Loader\ParamConfigurator;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;


/**
 * This class is automatically generated to help creating config.
 *
 * @experimental in 5.3
 */
class JkuFactoryConfig 
{
    private $enabled;
    private $client;
    private $requestFactory;
    
    /**
     * @default false
     * @param ParamConfigurator|bool $value
     * @return $this
     */
    public function enabled($value): self
    {
        $this->enabled = $value;
    
        return $this;
    }
    
    /**
     * HTTP Client used to retrieve key sets.
     * @default null
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function client($value): self
    {
        $this->client = $value;
    
        return $this;
    }
    
    /**
     * The request factory service.
     * @default null
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function requestFactory($value): self
    {
        $this->requestFactory = $value;
    
        return $this;
    }
    
    public function __construct(array $value = [])
    {
    
        if (isset($value['enabled'])) {
            $this->enabled = $value['enabled'];
            unset($value['enabled']);
        }
    
        if (isset($value['client'])) {
            $this->client = $value['client'];
            unset($value['client']);
        }
    
        if (isset($value['request_factory'])) {
            $this->requestFactory = $value['request_factory'];
            unset($value['request_factory']);
        }
    
        if ([] !== $value) {
            throw new InvalidConfigurationException(sprintf('The following keys are not supported by "%s": ', __CLASS__).implode(', ', array_keys($value)));
        }
    }
    
    
    public function toArray(): array
    {
        $output = [];
        if (null !== $this->enabled) {
            $output['enabled'] = $this->enabled;
        }
        if (null !== $this->client) {
            $output['client'] = $this->client;
        }
        if (null !== $this->requestFactory) {
            $output['request_factory'] = $this->requestFactory;
        }
    
        return $output;
    }
    

}
