<?php

namespace Symfony\Config\Jose;

require_once __DIR__.\DIRECTORY_SEPARATOR.'Jws'.\DIRECTORY_SEPARATOR.'BuildersConfig.php';
require_once __DIR__.\DIRECTORY_SEPARATOR.'Jws'.\DIRECTORY_SEPARATOR.'VerifiersConfig.php';
require_once __DIR__.\DIRECTORY_SEPARATOR.'Jws'.\DIRECTORY_SEPARATOR.'SerializersConfig.php';
require_once __DIR__.\DIRECTORY_SEPARATOR.'Jws'.\DIRECTORY_SEPARATOR.'LoadersConfig.php';

use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;


/**
 * This class is automatically generated to help creating config.
 *
 * @experimental in 5.3
 */
class JwsConfig 
{
    private $builders;
    private $verifiers;
    private $serializers;
    private $loaders;
    
    public function builders(string $name, array $value = []): \Symfony\Config\Jose\Jws\BuildersConfig
    {
        if (!isset($this->builders[$name])) {
            return $this->builders[$name] = new \Symfony\Config\Jose\Jws\BuildersConfig($value);
        }
        if ([] === $value) {
            return $this->builders[$name];
        }
    
        throw new InvalidConfigurationException('The node created by "builders()" has already been initialized. You cannot pass values the second time you call builders().');
    }
    
    public function verifiers(string $name, array $value = []): \Symfony\Config\Jose\Jws\VerifiersConfig
    {
        if (!isset($this->verifiers[$name])) {
            return $this->verifiers[$name] = new \Symfony\Config\Jose\Jws\VerifiersConfig($value);
        }
        if ([] === $value) {
            return $this->verifiers[$name];
        }
    
        throw new InvalidConfigurationException('The node created by "verifiers()" has already been initialized. You cannot pass values the second time you call verifiers().');
    }
    
    public function serializers(string $name, array $value = []): \Symfony\Config\Jose\Jws\SerializersConfig
    {
        if (!isset($this->serializers[$name])) {
            return $this->serializers[$name] = new \Symfony\Config\Jose\Jws\SerializersConfig($value);
        }
        if ([] === $value) {
            return $this->serializers[$name];
        }
    
        throw new InvalidConfigurationException('The node created by "serializers()" has already been initialized. You cannot pass values the second time you call serializers().');
    }
    
    public function loaders(string $name, array $value = []): \Symfony\Config\Jose\Jws\LoadersConfig
    {
        if (!isset($this->loaders[$name])) {
            return $this->loaders[$name] = new \Symfony\Config\Jose\Jws\LoadersConfig($value);
        }
        if ([] === $value) {
            return $this->loaders[$name];
        }
    
        throw new InvalidConfigurationException('The node created by "loaders()" has already been initialized. You cannot pass values the second time you call loaders().');
    }
    
    public function __construct(array $value = [])
    {
    
        if (isset($value['builders'])) {
            $this->builders = array_map(function ($v) { return new \Symfony\Config\Jose\Jws\BuildersConfig($v); }, $value['builders']);
            unset($value['builders']);
        }
    
        if (isset($value['verifiers'])) {
            $this->verifiers = array_map(function ($v) { return new \Symfony\Config\Jose\Jws\VerifiersConfig($v); }, $value['verifiers']);
            unset($value['verifiers']);
        }
    
        if (isset($value['serializers'])) {
            $this->serializers = array_map(function ($v) { return new \Symfony\Config\Jose\Jws\SerializersConfig($v); }, $value['serializers']);
            unset($value['serializers']);
        }
    
        if (isset($value['loaders'])) {
            $this->loaders = array_map(function ($v) { return new \Symfony\Config\Jose\Jws\LoadersConfig($v); }, $value['loaders']);
            unset($value['loaders']);
        }
    
        if ([] !== $value) {
            throw new InvalidConfigurationException(sprintf('The following keys are not supported by "%s": ', __CLASS__).implode(', ', array_keys($value)));
        }
    }
    
    
    public function toArray(): array
    {
        $output = [];
        if (null !== $this->builders) {
            $output['builders'] = array_map(function ($v) { return $v->toArray(); }, $this->builders);
        }
        if (null !== $this->verifiers) {
            $output['verifiers'] = array_map(function ($v) { return $v->toArray(); }, $this->verifiers);
        }
        if (null !== $this->serializers) {
            $output['serializers'] = array_map(function ($v) { return $v->toArray(); }, $this->serializers);
        }
        if (null !== $this->loaders) {
            $output['loaders'] = array_map(function ($v) { return $v->toArray(); }, $this->loaders);
        }
    
        return $output;
    }
    

}
