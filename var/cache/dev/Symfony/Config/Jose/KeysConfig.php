<?php

namespace Symfony\Config\Jose;

require_once __DIR__.\DIRECTORY_SEPARATOR.'KeysConfig'.\DIRECTORY_SEPARATOR.'FileConfig.php';
require_once __DIR__.\DIRECTORY_SEPARATOR.'KeysConfig'.\DIRECTORY_SEPARATOR.'P12Config.php';
require_once __DIR__.\DIRECTORY_SEPARATOR.'KeysConfig'.\DIRECTORY_SEPARATOR.'CertificateConfig.php';
require_once __DIR__.\DIRECTORY_SEPARATOR.'KeysConfig'.\DIRECTORY_SEPARATOR.'ValuesConfig.php';
require_once __DIR__.\DIRECTORY_SEPARATOR.'KeysConfig'.\DIRECTORY_SEPARATOR.'SecretConfig.php';
require_once __DIR__.\DIRECTORY_SEPARATOR.'KeysConfig'.\DIRECTORY_SEPARATOR.'JwkConfig.php';
require_once __DIR__.\DIRECTORY_SEPARATOR.'KeysConfig'.\DIRECTORY_SEPARATOR.'X5cConfig.php';
require_once __DIR__.\DIRECTORY_SEPARATOR.'KeysConfig'.\DIRECTORY_SEPARATOR.'JwksetConfig.php';

use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;


/**
 * This class is automatically generated to help creating config.
 *
 * @experimental in 5.3
 */
class KeysConfig 
{
    private $file;
    private $p12;
    private $certificate;
    private $values;
    private $secret;
    private $jwk;
    private $x5c;
    private $jwkset;
    
    public function file(array $value = []): \Symfony\Config\Jose\KeysConfig\FileConfig
    {
        if (null === $this->file) {
            $this->file = new \Symfony\Config\Jose\KeysConfig\FileConfig($value);
        } elseif ([] !== $value) {
            throw new InvalidConfigurationException('The node created by "file()" has already been initialized. You cannot pass values the second time you call file().');
        }
    
        return $this->file;
    }
    
    public function p12(array $value = []): \Symfony\Config\Jose\KeysConfig\P12Config
    {
        if (null === $this->p12) {
            $this->p12 = new \Symfony\Config\Jose\KeysConfig\P12Config($value);
        } elseif ([] !== $value) {
            throw new InvalidConfigurationException('The node created by "p12()" has already been initialized. You cannot pass values the second time you call p12().');
        }
    
        return $this->p12;
    }
    
    public function certificate(array $value = []): \Symfony\Config\Jose\KeysConfig\CertificateConfig
    {
        if (null === $this->certificate) {
            $this->certificate = new \Symfony\Config\Jose\KeysConfig\CertificateConfig($value);
        } elseif ([] !== $value) {
            throw new InvalidConfigurationException('The node created by "certificate()" has already been initialized. You cannot pass values the second time you call certificate().');
        }
    
        return $this->certificate;
    }
    
    public function values(array $value = []): \Symfony\Config\Jose\KeysConfig\ValuesConfig
    {
        if (null === $this->values) {
            $this->values = new \Symfony\Config\Jose\KeysConfig\ValuesConfig($value);
        } elseif ([] !== $value) {
            throw new InvalidConfigurationException('The node created by "values()" has already been initialized. You cannot pass values the second time you call values().');
        }
    
        return $this->values;
    }
    
    public function secret(array $value = []): \Symfony\Config\Jose\KeysConfig\SecretConfig
    {
        if (null === $this->secret) {
            $this->secret = new \Symfony\Config\Jose\KeysConfig\SecretConfig($value);
        } elseif ([] !== $value) {
            throw new InvalidConfigurationException('The node created by "secret()" has already been initialized. You cannot pass values the second time you call secret().');
        }
    
        return $this->secret;
    }
    
    public function jwk(array $value = []): \Symfony\Config\Jose\KeysConfig\JwkConfig
    {
        if (null === $this->jwk) {
            $this->jwk = new \Symfony\Config\Jose\KeysConfig\JwkConfig($value);
        } elseif ([] !== $value) {
            throw new InvalidConfigurationException('The node created by "jwk()" has already been initialized. You cannot pass values the second time you call jwk().');
        }
    
        return $this->jwk;
    }
    
    public function x5c(array $value = []): \Symfony\Config\Jose\KeysConfig\X5cConfig
    {
        if (null === $this->x5c) {
            $this->x5c = new \Symfony\Config\Jose\KeysConfig\X5cConfig($value);
        } elseif ([] !== $value) {
            throw new InvalidConfigurationException('The node created by "x5c()" has already been initialized. You cannot pass values the second time you call x5c().');
        }
    
        return $this->x5c;
    }
    
    public function jwkset(array $value = []): \Symfony\Config\Jose\KeysConfig\JwksetConfig
    {
        if (null === $this->jwkset) {
            $this->jwkset = new \Symfony\Config\Jose\KeysConfig\JwksetConfig($value);
        } elseif ([] !== $value) {
            throw new InvalidConfigurationException('The node created by "jwkset()" has already been initialized. You cannot pass values the second time you call jwkset().');
        }
    
        return $this->jwkset;
    }
    
    public function __construct(array $value = [])
    {
    
        if (isset($value['file'])) {
            $this->file = new \Symfony\Config\Jose\KeysConfig\FileConfig($value['file']);
            unset($value['file']);
        }
    
        if (isset($value['p12'])) {
            $this->p12 = new \Symfony\Config\Jose\KeysConfig\P12Config($value['p12']);
            unset($value['p12']);
        }
    
        if (isset($value['certificate'])) {
            $this->certificate = new \Symfony\Config\Jose\KeysConfig\CertificateConfig($value['certificate']);
            unset($value['certificate']);
        }
    
        if (isset($value['values'])) {
            $this->values = new \Symfony\Config\Jose\KeysConfig\ValuesConfig($value['values']);
            unset($value['values']);
        }
    
        if (isset($value['secret'])) {
            $this->secret = new \Symfony\Config\Jose\KeysConfig\SecretConfig($value['secret']);
            unset($value['secret']);
        }
    
        if (isset($value['jwk'])) {
            $this->jwk = new \Symfony\Config\Jose\KeysConfig\JwkConfig($value['jwk']);
            unset($value['jwk']);
        }
    
        if (isset($value['x5c'])) {
            $this->x5c = new \Symfony\Config\Jose\KeysConfig\X5cConfig($value['x5c']);
            unset($value['x5c']);
        }
    
        if (isset($value['jwkset'])) {
            $this->jwkset = new \Symfony\Config\Jose\KeysConfig\JwksetConfig($value['jwkset']);
            unset($value['jwkset']);
        }
    
        if ([] !== $value) {
            throw new InvalidConfigurationException(sprintf('The following keys are not supported by "%s": ', __CLASS__).implode(', ', array_keys($value)));
        }
    }
    
    
    public function toArray(): array
    {
        $output = [];
        if (null !== $this->file) {
            $output['file'] = $this->file->toArray();
        }
        if (null !== $this->p12) {
            $output['p12'] = $this->p12->toArray();
        }
        if (null !== $this->certificate) {
            $output['certificate'] = $this->certificate->toArray();
        }
        if (null !== $this->values) {
            $output['values'] = $this->values->toArray();
        }
        if (null !== $this->secret) {
            $output['secret'] = $this->secret->toArray();
        }
        if (null !== $this->jwk) {
            $output['jwk'] = $this->jwk->toArray();
        }
        if (null !== $this->x5c) {
            $output['x5c'] = $this->x5c->toArray();
        }
        if (null !== $this->jwkset) {
            $output['jwkset'] = $this->jwkset->toArray();
        }
    
        return $output;
    }
    

}
