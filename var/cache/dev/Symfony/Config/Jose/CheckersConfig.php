<?php

namespace Symfony\Config\Jose;

require_once __DIR__.\DIRECTORY_SEPARATOR.'Checkers'.\DIRECTORY_SEPARATOR.'ClaimsConfig.php';
require_once __DIR__.\DIRECTORY_SEPARATOR.'Checkers'.\DIRECTORY_SEPARATOR.'HeadersConfig.php';

use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;


/**
 * This class is automatically generated to help creating config.
 *
 * @experimental in 5.3
 */
class CheckersConfig 
{
    private $claims;
    private $headers;
    
    public function claims(string $name, array $value = []): \Symfony\Config\Jose\Checkers\ClaimsConfig
    {
        if (!isset($this->claims[$name])) {
            return $this->claims[$name] = new \Symfony\Config\Jose\Checkers\ClaimsConfig($value);
        }
        if ([] === $value) {
            return $this->claims[$name];
        }
    
        throw new InvalidConfigurationException('The node created by "claims()" has already been initialized. You cannot pass values the second time you call claims().');
    }
    
    public function headers(string $name, array $value = []): \Symfony\Config\Jose\Checkers\HeadersConfig
    {
        if (!isset($this->headers[$name])) {
            return $this->headers[$name] = new \Symfony\Config\Jose\Checkers\HeadersConfig($value);
        }
        if ([] === $value) {
            return $this->headers[$name];
        }
    
        throw new InvalidConfigurationException('The node created by "headers()" has already been initialized. You cannot pass values the second time you call headers().');
    }
    
    public function __construct(array $value = [])
    {
    
        if (isset($value['claims'])) {
            $this->claims = array_map(function ($v) { return new \Symfony\Config\Jose\Checkers\ClaimsConfig($v); }, $value['claims']);
            unset($value['claims']);
        }
    
        if (isset($value['headers'])) {
            $this->headers = array_map(function ($v) { return new \Symfony\Config\Jose\Checkers\HeadersConfig($v); }, $value['headers']);
            unset($value['headers']);
        }
    
        if ([] !== $value) {
            throw new InvalidConfigurationException(sprintf('The following keys are not supported by "%s": ', __CLASS__).implode(', ', array_keys($value)));
        }
    }
    
    
    public function toArray(): array
    {
        $output = [];
        if (null !== $this->claims) {
            $output['claims'] = array_map(function ($v) { return $v->toArray(); }, $this->claims);
        }
        if (null !== $this->headers) {
            $output['headers'] = array_map(function ($v) { return $v->toArray(); }, $this->headers);
        }
    
        return $output;
    }
    

}
