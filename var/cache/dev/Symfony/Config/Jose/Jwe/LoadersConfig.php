<?php

namespace Symfony\Config\Jose\Jwe;


use Symfony\Component\Config\Loader\ParamConfigurator;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;


/**
 * This class is automatically generated to help creating config.
 *
 * @experimental in 5.3
 */
class LoadersConfig 
{
    private $isPublic;
    private $keyEncryptionAlgorithms;
    private $contentEncryptionAlgorithms;
    private $compressionMethods;
    private $serializers;
    private $headerCheckers;
    private $tags;
    
    /**
     * If true, the service will be public, else private.
     * @default true
     * @param ParamConfigurator|bool $value
     * @return $this
     */
    public function isPublic($value): self
    {
        $this->isPublic = $value;
    
        return $this;
    }
    
    /**
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function keyEncryptionAlgorithms(string $name, $value): self
    {
        $this->keyEncryptionAlgorithms[$name] = $value;
    
        return $this;
    }
    
    /**
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function contentEncryptionAlgorithms(string $name, $value): self
    {
        $this->contentEncryptionAlgorithms[$name] = $value;
    
        return $this;
    }
    
    /**
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function compressionMethods(string $name, $value): self
    {
        $this->compressionMethods[$name] = $value;
    
        return $this;
    }
    
    /**
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function serializers(string $name, $value): self
    {
        $this->serializers[$name] = $value;
    
        return $this;
    }
    
    /**
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function headerCheckers(string $name, $value): self
    {
        $this->headerCheckers[$name] = $value;
    
        return $this;
    }
    
    /**
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function tags(string $name, $value): self
    {
        $this->tags[$name] = $value;
    
        return $this;
    }
    
    public function __construct(array $value = [])
    {
    
        if (isset($value['is_public'])) {
            $this->isPublic = $value['is_public'];
            unset($value['is_public']);
        }
    
        if (isset($value['key_encryption_algorithms'])) {
            $this->keyEncryptionAlgorithms = $value['key_encryption_algorithms'];
            unset($value['key_encryption_algorithms']);
        }
    
        if (isset($value['content_encryption_algorithms'])) {
            $this->contentEncryptionAlgorithms = $value['content_encryption_algorithms'];
            unset($value['content_encryption_algorithms']);
        }
    
        if (isset($value['compression_methods'])) {
            $this->compressionMethods = $value['compression_methods'];
            unset($value['compression_methods']);
        }
    
        if (isset($value['serializers'])) {
            $this->serializers = $value['serializers'];
            unset($value['serializers']);
        }
    
        if (isset($value['header_checkers'])) {
            $this->headerCheckers = $value['header_checkers'];
            unset($value['header_checkers']);
        }
    
        if (isset($value['tags'])) {
            $this->tags = $value['tags'];
            unset($value['tags']);
        }
    
        if ([] !== $value) {
            throw new InvalidConfigurationException(sprintf('The following keys are not supported by "%s": ', __CLASS__).implode(', ', array_keys($value)));
        }
    }
    
    
    public function toArray(): array
    {
        $output = [];
        if (null !== $this->isPublic) {
            $output['is_public'] = $this->isPublic;
        }
        if (null !== $this->keyEncryptionAlgorithms) {
            $output['key_encryption_algorithms'] = $this->keyEncryptionAlgorithms;
        }
        if (null !== $this->contentEncryptionAlgorithms) {
            $output['content_encryption_algorithms'] = $this->contentEncryptionAlgorithms;
        }
        if (null !== $this->compressionMethods) {
            $output['compression_methods'] = $this->compressionMethods;
        }
        if (null !== $this->serializers) {
            $output['serializers'] = $this->serializers;
        }
        if (null !== $this->headerCheckers) {
            $output['header_checkers'] = $this->headerCheckers;
        }
        if (null !== $this->tags) {
            $output['tags'] = $this->tags;
        }
    
        return $output;
    }
    

}
