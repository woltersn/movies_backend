<?php

namespace Symfony\Config\Jose;

require_once __DIR__.\DIRECTORY_SEPARATOR.'Jwe'.\DIRECTORY_SEPARATOR.'BuildersConfig.php';
require_once __DIR__.\DIRECTORY_SEPARATOR.'Jwe'.\DIRECTORY_SEPARATOR.'DecryptersConfig.php';
require_once __DIR__.\DIRECTORY_SEPARATOR.'Jwe'.\DIRECTORY_SEPARATOR.'SerializersConfig.php';
require_once __DIR__.\DIRECTORY_SEPARATOR.'Jwe'.\DIRECTORY_SEPARATOR.'LoadersConfig.php';

use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;


/**
 * This class is automatically generated to help creating config.
 *
 * @experimental in 5.3
 */
class JweConfig 
{
    private $builders;
    private $decrypters;
    private $serializers;
    private $loaders;
    
    public function builders(string $name, array $value = []): \Symfony\Config\Jose\Jwe\BuildersConfig
    {
        if (!isset($this->builders[$name])) {
            return $this->builders[$name] = new \Symfony\Config\Jose\Jwe\BuildersConfig($value);
        }
        if ([] === $value) {
            return $this->builders[$name];
        }
    
        throw new InvalidConfigurationException('The node created by "builders()" has already been initialized. You cannot pass values the second time you call builders().');
    }
    
    public function decrypters(string $name, array $value = []): \Symfony\Config\Jose\Jwe\DecryptersConfig
    {
        if (!isset($this->decrypters[$name])) {
            return $this->decrypters[$name] = new \Symfony\Config\Jose\Jwe\DecryptersConfig($value);
        }
        if ([] === $value) {
            return $this->decrypters[$name];
        }
    
        throw new InvalidConfigurationException('The node created by "decrypters()" has already been initialized. You cannot pass values the second time you call decrypters().');
    }
    
    public function serializers(string $name, array $value = []): \Symfony\Config\Jose\Jwe\SerializersConfig
    {
        if (!isset($this->serializers[$name])) {
            return $this->serializers[$name] = new \Symfony\Config\Jose\Jwe\SerializersConfig($value);
        }
        if ([] === $value) {
            return $this->serializers[$name];
        }
    
        throw new InvalidConfigurationException('The node created by "serializers()" has already been initialized. You cannot pass values the second time you call serializers().');
    }
    
    public function loaders(string $name, array $value = []): \Symfony\Config\Jose\Jwe\LoadersConfig
    {
        if (!isset($this->loaders[$name])) {
            return $this->loaders[$name] = new \Symfony\Config\Jose\Jwe\LoadersConfig($value);
        }
        if ([] === $value) {
            return $this->loaders[$name];
        }
    
        throw new InvalidConfigurationException('The node created by "loaders()" has already been initialized. You cannot pass values the second time you call loaders().');
    }
    
    public function __construct(array $value = [])
    {
    
        if (isset($value['builders'])) {
            $this->builders = array_map(function ($v) { return new \Symfony\Config\Jose\Jwe\BuildersConfig($v); }, $value['builders']);
            unset($value['builders']);
        }
    
        if (isset($value['decrypters'])) {
            $this->decrypters = array_map(function ($v) { return new \Symfony\Config\Jose\Jwe\DecryptersConfig($v); }, $value['decrypters']);
            unset($value['decrypters']);
        }
    
        if (isset($value['serializers'])) {
            $this->serializers = array_map(function ($v) { return new \Symfony\Config\Jose\Jwe\SerializersConfig($v); }, $value['serializers']);
            unset($value['serializers']);
        }
    
        if (isset($value['loaders'])) {
            $this->loaders = array_map(function ($v) { return new \Symfony\Config\Jose\Jwe\LoadersConfig($v); }, $value['loaders']);
            unset($value['loaders']);
        }
    
        if ([] !== $value) {
            throw new InvalidConfigurationException(sprintf('The following keys are not supported by "%s": ', __CLASS__).implode(', ', array_keys($value)));
        }
    }
    
    
    public function toArray(): array
    {
        $output = [];
        if (null !== $this->builders) {
            $output['builders'] = array_map(function ($v) { return $v->toArray(); }, $this->builders);
        }
        if (null !== $this->decrypters) {
            $output['decrypters'] = array_map(function ($v) { return $v->toArray(); }, $this->decrypters);
        }
        if (null !== $this->serializers) {
            $output['serializers'] = array_map(function ($v) { return $v->toArray(); }, $this->serializers);
        }
        if (null !== $this->loaders) {
            $output['loaders'] = array_map(function ($v) { return $v->toArray(); }, $this->loaders);
        }
    
        return $output;
    }
    

}
