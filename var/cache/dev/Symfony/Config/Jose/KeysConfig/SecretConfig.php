<?php

namespace Symfony\Config\Jose\KeysConfig;


use Symfony\Component\Config\Loader\ParamConfigurator;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;


/**
 * This class is automatically generated to help creating config.
 *
 * @experimental in 5.3
 */
class SecretConfig 
{
    private $isPublic;
    private $tags;
    private $secret;
    private $additionalValues;
    
    /**
     * If true, the service will be public, else private.
     * @default true
     * @param ParamConfigurator|bool $value
     * @return $this
     */
    public function isPublic($value): self
    {
        $this->isPublic = $value;
    
        return $this;
    }
    
    /**
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function tags(string $name, $value): self
    {
        $this->tags[$name] = $value;
    
        return $this;
    }
    
    /**
     * The shared secret.
     * @default null
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function secret($value): self
    {
        $this->secret = $value;
    
        return $this;
    }
    
    /**
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function additionalValues(string $key, $value): self
    {
        $this->additionalValues[$key] = $value;
    
        return $this;
    }
    
    public function __construct(array $value = [])
    {
    
        if (isset($value['is_public'])) {
            $this->isPublic = $value['is_public'];
            unset($value['is_public']);
        }
    
        if (isset($value['tags'])) {
            $this->tags = $value['tags'];
            unset($value['tags']);
        }
    
        if (isset($value['secret'])) {
            $this->secret = $value['secret'];
            unset($value['secret']);
        }
    
        if (isset($value['additional_values'])) {
            $this->additionalValues = $value['additional_values'];
            unset($value['additional_values']);
        }
    
        if ([] !== $value) {
            throw new InvalidConfigurationException(sprintf('The following keys are not supported by "%s": ', __CLASS__).implode(', ', array_keys($value)));
        }
    }
    
    
    public function toArray(): array
    {
        $output = [];
        if (null !== $this->isPublic) {
            $output['is_public'] = $this->isPublic;
        }
        if (null !== $this->tags) {
            $output['tags'] = $this->tags;
        }
        if (null !== $this->secret) {
            $output['secret'] = $this->secret;
        }
        if (null !== $this->additionalValues) {
            $output['additional_values'] = $this->additionalValues;
        }
    
        return $output;
    }
    

}
