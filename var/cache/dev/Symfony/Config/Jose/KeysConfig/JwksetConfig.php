<?php

namespace Symfony\Config\Jose\KeysConfig;


use Symfony\Component\Config\Loader\ParamConfigurator;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;


/**
 * This class is automatically generated to help creating config.
 *
 * @experimental in 5.3
 */
class JwksetConfig 
{
    private $isPublic;
    private $tags;
    private $keySet;
    private $index;
    
    /**
     * If true, the service will be public, else private.
     * @default true
     * @param ParamConfigurator|bool $value
     * @return $this
     */
    public function isPublic($value): self
    {
        $this->isPublic = $value;
    
        return $this;
    }
    
    /**
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function tags(string $name, $value): self
    {
        $this->tags[$name] = $value;
    
        return $this;
    }
    
    /**
     * The key set service.
     * @default null
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function keySet($value): self
    {
        $this->keySet = $value;
    
        return $this;
    }
    
    /**
     * The index of the key in the key set.
     * @default null
     * @param ParamConfigurator|int $value
     * @return $this
     */
    public function index($value): self
    {
        $this->index = $value;
    
        return $this;
    }
    
    public function __construct(array $value = [])
    {
    
        if (isset($value['is_public'])) {
            $this->isPublic = $value['is_public'];
            unset($value['is_public']);
        }
    
        if (isset($value['tags'])) {
            $this->tags = $value['tags'];
            unset($value['tags']);
        }
    
        if (isset($value['key_set'])) {
            $this->keySet = $value['key_set'];
            unset($value['key_set']);
        }
    
        if (isset($value['index'])) {
            $this->index = $value['index'];
            unset($value['index']);
        }
    
        if ([] !== $value) {
            throw new InvalidConfigurationException(sprintf('The following keys are not supported by "%s": ', __CLASS__).implode(', ', array_keys($value)));
        }
    }
    
    
    public function toArray(): array
    {
        $output = [];
        if (null !== $this->isPublic) {
            $output['is_public'] = $this->isPublic;
        }
        if (null !== $this->tags) {
            $output['tags'] = $this->tags;
        }
        if (null !== $this->keySet) {
            $output['key_set'] = $this->keySet;
        }
        if (null !== $this->index) {
            $output['index'] = $this->index;
        }
    
        return $output;
    }
    

}
