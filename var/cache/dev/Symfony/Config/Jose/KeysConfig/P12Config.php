<?php

namespace Symfony\Config\Jose\KeysConfig;


use Symfony\Component\Config\Loader\ParamConfigurator;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;


/**
 * This class is automatically generated to help creating config.
 *
 * @experimental in 5.3
 */
class P12Config 
{
    private $isPublic;
    private $tags;
    private $path;
    private $password;
    private $additionalValues;
    
    /**
     * If true, the service will be public, else private.
     * @default true
     * @param ParamConfigurator|bool $value
     * @return $this
     */
    public function isPublic($value): self
    {
        $this->isPublic = $value;
    
        return $this;
    }
    
    /**
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function tags(string $name, $value): self
    {
        $this->tags[$name] = $value;
    
        return $this;
    }
    
    /**
     * Path of the key file.
     * @default null
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function path($value): self
    {
        $this->path = $value;
    
        return $this;
    }
    
    /**
     * Password used to decrypt the key (optional).
     * @default null
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function password($value): self
    {
        $this->password = $value;
    
        return $this;
    }
    
    /**
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function additionalValues(string $key, $value): self
    {
        $this->additionalValues[$key] = $value;
    
        return $this;
    }
    
    public function __construct(array $value = [])
    {
    
        if (isset($value['is_public'])) {
            $this->isPublic = $value['is_public'];
            unset($value['is_public']);
        }
    
        if (isset($value['tags'])) {
            $this->tags = $value['tags'];
            unset($value['tags']);
        }
    
        if (isset($value['path'])) {
            $this->path = $value['path'];
            unset($value['path']);
        }
    
        if (isset($value['password'])) {
            $this->password = $value['password'];
            unset($value['password']);
        }
    
        if (isset($value['additional_values'])) {
            $this->additionalValues = $value['additional_values'];
            unset($value['additional_values']);
        }
    
        if ([] !== $value) {
            throw new InvalidConfigurationException(sprintf('The following keys are not supported by "%s": ', __CLASS__).implode(', ', array_keys($value)));
        }
    }
    
    
    public function toArray(): array
    {
        $output = [];
        if (null !== $this->isPublic) {
            $output['is_public'] = $this->isPublic;
        }
        if (null !== $this->tags) {
            $output['tags'] = $this->tags;
        }
        if (null !== $this->path) {
            $output['path'] = $this->path;
        }
        if (null !== $this->password) {
            $output['password'] = $this->password;
        }
        if (null !== $this->additionalValues) {
            $output['additional_values'] = $this->additionalValues;
        }
    
        return $output;
    }
    

}
