<?php

namespace Symfony\Config;

require_once __DIR__.\DIRECTORY_SEPARATOR.'Jose'.\DIRECTORY_SEPARATOR.'CheckersConfig.php';
require_once __DIR__.\DIRECTORY_SEPARATOR.'Jose'.\DIRECTORY_SEPARATOR.'JwsConfig.php';
require_once __DIR__.\DIRECTORY_SEPARATOR.'Jose'.\DIRECTORY_SEPARATOR.'JweConfig.php';
require_once __DIR__.\DIRECTORY_SEPARATOR.'Jose'.\DIRECTORY_SEPARATOR.'KeySetsConfig.php';
require_once __DIR__.\DIRECTORY_SEPARATOR.'Jose'.\DIRECTORY_SEPARATOR.'KeysConfig.php';
require_once __DIR__.\DIRECTORY_SEPARATOR.'Jose'.\DIRECTORY_SEPARATOR.'JwkUrisConfig.php';
require_once __DIR__.\DIRECTORY_SEPARATOR.'Jose'.\DIRECTORY_SEPARATOR.'JkuFactoryConfig.php';

use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;


/**
 * This class is automatically generated to help creating config.
 *
 * @experimental in 5.3
 */
class JoseConfig implements \Symfony\Component\Config\Builder\ConfigBuilderInterface
{
    private $checkers;
    private $jws;
    private $jwe;
    private $keySets;
    private $keys;
    private $jwkUris;
    private $jkuFactory;
    
    public function checkers(array $value = []): \Symfony\Config\Jose\CheckersConfig
    {
        if (null === $this->checkers) {
            $this->checkers = new \Symfony\Config\Jose\CheckersConfig($value);
        } elseif ([] !== $value) {
            throw new InvalidConfigurationException('The node created by "checkers()" has already been initialized. You cannot pass values the second time you call checkers().');
        }
    
        return $this->checkers;
    }
    
    public function jws(array $value = []): \Symfony\Config\Jose\JwsConfig
    {
        if (null === $this->jws) {
            $this->jws = new \Symfony\Config\Jose\JwsConfig($value);
        } elseif ([] !== $value) {
            throw new InvalidConfigurationException('The node created by "jws()" has already been initialized. You cannot pass values the second time you call jws().');
        }
    
        return $this->jws;
    }
    
    public function jwe(array $value = []): \Symfony\Config\Jose\JweConfig
    {
        if (null === $this->jwe) {
            $this->jwe = new \Symfony\Config\Jose\JweConfig($value);
        } elseif ([] !== $value) {
            throw new InvalidConfigurationException('The node created by "jwe()" has already been initialized. You cannot pass values the second time you call jwe().');
        }
    
        return $this->jwe;
    }
    
    public function keySets(string $name, array $value = []): \Symfony\Config\Jose\KeySetsConfig
    {
        if (!isset($this->keySets[$name])) {
            return $this->keySets[$name] = new \Symfony\Config\Jose\KeySetsConfig($value);
        }
        if ([] === $value) {
            return $this->keySets[$name];
        }
    
        throw new InvalidConfigurationException('The node created by "keySets()" has already been initialized. You cannot pass values the second time you call keySets().');
    }
    
    public function keys(string $name, array $value = []): \Symfony\Config\Jose\KeysConfig
    {
        if (!isset($this->keys[$name])) {
            return $this->keys[$name] = new \Symfony\Config\Jose\KeysConfig($value);
        }
        if ([] === $value) {
            return $this->keys[$name];
        }
    
        throw new InvalidConfigurationException('The node created by "keys()" has already been initialized. You cannot pass values the second time you call keys().');
    }
    
    public function jwkUris(string $name, array $value = []): \Symfony\Config\Jose\JwkUrisConfig
    {
        if (!isset($this->jwkUris[$name])) {
            return $this->jwkUris[$name] = new \Symfony\Config\Jose\JwkUrisConfig($value);
        }
        if ([] === $value) {
            return $this->jwkUris[$name];
        }
    
        throw new InvalidConfigurationException('The node created by "jwkUris()" has already been initialized. You cannot pass values the second time you call jwkUris().');
    }
    
    public function jkuFactory(array $value = []): \Symfony\Config\Jose\JkuFactoryConfig
    {
        if (null === $this->jkuFactory) {
            $this->jkuFactory = new \Symfony\Config\Jose\JkuFactoryConfig($value);
        } elseif ([] !== $value) {
            throw new InvalidConfigurationException('The node created by "jkuFactory()" has already been initialized. You cannot pass values the second time you call jkuFactory().');
        }
    
        return $this->jkuFactory;
    }
    
    public function getExtensionAlias(): string
    {
        return 'jose';
    }
            
    
    public function __construct(array $value = [])
    {
    
        if (isset($value['checkers'])) {
            $this->checkers = new \Symfony\Config\Jose\CheckersConfig($value['checkers']);
            unset($value['checkers']);
        }
    
        if (isset($value['jws'])) {
            $this->jws = new \Symfony\Config\Jose\JwsConfig($value['jws']);
            unset($value['jws']);
        }
    
        if (isset($value['jwe'])) {
            $this->jwe = new \Symfony\Config\Jose\JweConfig($value['jwe']);
            unset($value['jwe']);
        }
    
        if (isset($value['key_sets'])) {
            $this->keySets = array_map(function ($v) { return new \Symfony\Config\Jose\KeySetsConfig($v); }, $value['key_sets']);
            unset($value['key_sets']);
        }
    
        if (isset($value['keys'])) {
            $this->keys = array_map(function ($v) { return new \Symfony\Config\Jose\KeysConfig($v); }, $value['keys']);
            unset($value['keys']);
        }
    
        if (isset($value['jwk_uris'])) {
            $this->jwkUris = array_map(function ($v) { return new \Symfony\Config\Jose\JwkUrisConfig($v); }, $value['jwk_uris']);
            unset($value['jwk_uris']);
        }
    
        if (isset($value['jku_factory'])) {
            $this->jkuFactory = new \Symfony\Config\Jose\JkuFactoryConfig($value['jku_factory']);
            unset($value['jku_factory']);
        }
    
        if ([] !== $value) {
            throw new InvalidConfigurationException(sprintf('The following keys are not supported by "%s": ', __CLASS__).implode(', ', array_keys($value)));
        }
    }
    
    
    public function toArray(): array
    {
        $output = [];
        if (null !== $this->checkers) {
            $output['checkers'] = $this->checkers->toArray();
        }
        if (null !== $this->jws) {
            $output['jws'] = $this->jws->toArray();
        }
        if (null !== $this->jwe) {
            $output['jwe'] = $this->jwe->toArray();
        }
        if (null !== $this->keySets) {
            $output['key_sets'] = array_map(function ($v) { return $v->toArray(); }, $this->keySets);
        }
        if (null !== $this->keys) {
            $output['keys'] = array_map(function ($v) { return $v->toArray(); }, $this->keys);
        }
        if (null !== $this->jwkUris) {
            $output['jwk_uris'] = array_map(function ($v) { return $v->toArray(); }, $this->jwkUris);
        }
        if (null !== $this->jkuFactory) {
            $output['jku_factory'] = $this->jkuFactory->toArray();
        }
    
        return $output;
    }
    

}
