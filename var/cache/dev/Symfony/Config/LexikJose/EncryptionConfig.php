<?php

namespace Symfony\Config\LexikJose;


use Symfony\Component\Config\Loader\ParamConfigurator;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;


/**
 * This class is automatically generated to help creating config.
 *
 * @experimental in 5.3
 */
class EncryptionConfig 
{
    private $enabled;
    private $keySet;
    private $keyIndex;
    private $keyEncryptionAlgorithm;
    private $contentEncryptionAlgorithm;
    
    /**
     * @default false
     * @param ParamConfigurator|bool $value
     * @return $this
     */
    public function enabled($value): self
    {
        $this->enabled = $value;
    
        return $this;
    }
    
    /**
     * Private/ Shared keys used by this server to decrypt the tokens. Must be a JWKSet object.
     * @default null
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function keySet($value): self
    {
        $this->keySet = $value;
    
        return $this;
    }
    
    /**
     * Index of the key in the key set used to encrypt the tokens. Could be an integer or the key ID.
     * @default null
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function keyIndex($value): self
    {
        $this->keyIndex = $value;
    
        return $this;
    }
    
    /**
     * Key encryption algorithm used to encrypt the tokens.
     * @default null
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function keyEncryptionAlgorithm($value): self
    {
        $this->keyEncryptionAlgorithm = $value;
    
        return $this;
    }
    
    /**
     * Content encryption algorithm used to encrypt the tokens.
     * @default null
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function contentEncryptionAlgorithm($value): self
    {
        $this->contentEncryptionAlgorithm = $value;
    
        return $this;
    }
    
    public function __construct(array $value = [])
    {
    
        if (isset($value['enabled'])) {
            $this->enabled = $value['enabled'];
            unset($value['enabled']);
        }
    
        if (isset($value['key_set'])) {
            $this->keySet = $value['key_set'];
            unset($value['key_set']);
        }
    
        if (isset($value['key_index'])) {
            $this->keyIndex = $value['key_index'];
            unset($value['key_index']);
        }
    
        if (isset($value['key_encryption_algorithm'])) {
            $this->keyEncryptionAlgorithm = $value['key_encryption_algorithm'];
            unset($value['key_encryption_algorithm']);
        }
    
        if (isset($value['content_encryption_algorithm'])) {
            $this->contentEncryptionAlgorithm = $value['content_encryption_algorithm'];
            unset($value['content_encryption_algorithm']);
        }
    
        if ([] !== $value) {
            throw new InvalidConfigurationException(sprintf('The following keys are not supported by "%s": ', __CLASS__).implode(', ', array_keys($value)));
        }
    }
    
    
    public function toArray(): array
    {
        $output = [];
        if (null !== $this->enabled) {
            $output['enabled'] = $this->enabled;
        }
        if (null !== $this->keySet) {
            $output['key_set'] = $this->keySet;
        }
        if (null !== $this->keyIndex) {
            $output['key_index'] = $this->keyIndex;
        }
        if (null !== $this->keyEncryptionAlgorithm) {
            $output['key_encryption_algorithm'] = $this->keyEncryptionAlgorithm;
        }
        if (null !== $this->contentEncryptionAlgorithm) {
            $output['content_encryption_algorithm'] = $this->contentEncryptionAlgorithm;
        }
    
        return $output;
    }
    

}
