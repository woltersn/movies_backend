<?php

namespace Symfony\Config;

require_once __DIR__.\DIRECTORY_SEPARATOR.'LexikJose'.\DIRECTORY_SEPARATOR.'KeySetRemoteConfig.php';
require_once __DIR__.\DIRECTORY_SEPARATOR.'LexikJose'.\DIRECTORY_SEPARATOR.'EncryptionConfig.php';

use Symfony\Component\Config\Loader\ParamConfigurator;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;


/**
 * This class is automatically generated to help creating config.
 *
 * @experimental in 5.3
 */
class LexikJoseConfig implements \Symfony\Component\Config\Builder\ConfigBuilderInterface
{
    private $serverName;
    private $audience;
    private $ttl;
    private $keySet;
    private $keySetRemote;
    private $keyIndex;
    private $signatureAlgorithm;
    private $claimChecked;
    private $mandatoryClaims;
    private $encryption;
    
    /**
     * The name of the server. The recommended value is the server URL. This value will be used to check the issuer of the token.
     * @default null
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function serverName($value): self
    {
        $this->serverName = $value;
    
        return $this;
    }
    
    /**
     * The audience of the token. If not set `server_name` will be used.
     * @default null
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function audience($value): self
    {
        $this->audience = $value;
    
        return $this;
    }
    
    /**
     * The lifetime of a token (in second). For security reasons, a value below 1 hour (3600 sec) is recommended.
     * @default 1800
     * @param ParamConfigurator|int $value
     * @return $this
     */
    public function ttl($value): self
    {
        $this->ttl = $value;
    
        return $this;
    }
    
    /**
     * Private/Shared keys used by this server to validate signed tokens. Must be a JWKSet object.
     * @default null
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function keySet($value): self
    {
        $this->keySet = $value;
    
        return $this;
    }
    
    public function keySetRemote(array $value = []): \Symfony\Config\LexikJose\KeySetRemoteConfig
    {
        if (null === $this->keySetRemote) {
            $this->keySetRemote = new \Symfony\Config\LexikJose\KeySetRemoteConfig($value);
        } elseif ([] !== $value) {
            throw new InvalidConfigurationException('The node created by "keySetRemote()" has already been initialized. You cannot pass values the second time you call keySetRemote().');
        }
    
        return $this->keySetRemote;
    }
    
    /**
     * Index of the key in the key set used to sign the tokens. Could be an integer or the key ID.
     * @default null
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function keyIndex($value): self
    {
        $this->keyIndex = $value;
    
        return $this;
    }
    
    /**
     * Signature algorithm used to sign the tokens.
     * @default null
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function signatureAlgorithm($value): self
    {
        $this->signatureAlgorithm = $value;
    
        return $this;
    }
    
    /**
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function claimChecked(string $name, $value): self
    {
        $this->claimChecked[$name] = $value;
    
        return $this;
    }
    
    /**
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function mandatoryClaims(string $name, $value): self
    {
        $this->mandatoryClaims[$name] = $value;
    
        return $this;
    }
    
    public function encryption(array $value = []): \Symfony\Config\LexikJose\EncryptionConfig
    {
        if (null === $this->encryption) {
            $this->encryption = new \Symfony\Config\LexikJose\EncryptionConfig($value);
        } elseif ([] !== $value) {
            throw new InvalidConfigurationException('The node created by "encryption()" has already been initialized. You cannot pass values the second time you call encryption().');
        }
    
        return $this->encryption;
    }
    
    public function getExtensionAlias(): string
    {
        return 'lexik_jose';
    }
            
    
    public function __construct(array $value = [])
    {
    
        if (isset($value['server_name'])) {
            $this->serverName = $value['server_name'];
            unset($value['server_name']);
        }
    
        if (isset($value['audience'])) {
            $this->audience = $value['audience'];
            unset($value['audience']);
        }
    
        if (isset($value['ttl'])) {
            $this->ttl = $value['ttl'];
            unset($value['ttl']);
        }
    
        if (isset($value['key_set'])) {
            $this->keySet = $value['key_set'];
            unset($value['key_set']);
        }
    
        if (isset($value['key_set_remote'])) {
            $this->keySetRemote = new \Symfony\Config\LexikJose\KeySetRemoteConfig($value['key_set_remote']);
            unset($value['key_set_remote']);
        }
    
        if (isset($value['key_index'])) {
            $this->keyIndex = $value['key_index'];
            unset($value['key_index']);
        }
    
        if (isset($value['signature_algorithm'])) {
            $this->signatureAlgorithm = $value['signature_algorithm'];
            unset($value['signature_algorithm']);
        }
    
        if (isset($value['claim_checked'])) {
            $this->claimChecked = $value['claim_checked'];
            unset($value['claim_checked']);
        }
    
        if (isset($value['mandatory_claims'])) {
            $this->mandatoryClaims = $value['mandatory_claims'];
            unset($value['mandatory_claims']);
        }
    
        if (isset($value['encryption'])) {
            $this->encryption = new \Symfony\Config\LexikJose\EncryptionConfig($value['encryption']);
            unset($value['encryption']);
        }
    
        if ([] !== $value) {
            throw new InvalidConfigurationException(sprintf('The following keys are not supported by "%s": ', __CLASS__).implode(', ', array_keys($value)));
        }
    }
    
    
    public function toArray(): array
    {
        $output = [];
        if (null !== $this->serverName) {
            $output['server_name'] = $this->serverName;
        }
        if (null !== $this->audience) {
            $output['audience'] = $this->audience;
        }
        if (null !== $this->ttl) {
            $output['ttl'] = $this->ttl;
        }
        if (null !== $this->keySet) {
            $output['key_set'] = $this->keySet;
        }
        if (null !== $this->keySetRemote) {
            $output['key_set_remote'] = $this->keySetRemote->toArray();
        }
        if (null !== $this->keyIndex) {
            $output['key_index'] = $this->keyIndex;
        }
        if (null !== $this->signatureAlgorithm) {
            $output['signature_algorithm'] = $this->signatureAlgorithm;
        }
        if (null !== $this->claimChecked) {
            $output['claim_checked'] = $this->claimChecked;
        }
        if (null !== $this->mandatoryClaims) {
            $output['mandatory_claims'] = $this->mandatoryClaims;
        }
        if (null !== $this->encryption) {
            $output['encryption'] = $this->encryption->toArray();
        }
    
        return $output;
    }
    

}
